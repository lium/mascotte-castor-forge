# Mascotte Castor Forge 

## Name
Mascotte Castor pour la Forge de l'Education Nationale.\
Son petit nom est "Komit".

## Description
Nous avons créer cette mascotte pour la Forge de l'Education Nationale. \
C'est un projet personnel, effectué dans le cadre de l'Habilitation à Diriger des Recherches de Iza Marfisi, en 2023. 
Le dernier chapitre parle de la forge et de comment elle pourrait fédérer les acteurs de la recherche, de l'enseignement et des entreprises EdTech. \
Le castor et son crayon en papier représentent la co-construction de logiciels libres pour l’éducation et la formation professionnelle. 
Nous souhaitions véhiculer les concepts de l’école et la création effaçable et éditable (modification de code).

<img src="https://lium.univ-lemans.fr/wp-content/uploads/2023/08/Illustration_mascotte_forge.png" width="300">

Nous proposons égallement Komit dans d'autre pause pour illustrer des projets ou des réussites. Nous avons créer ses variantes dans le cadre du GTNum Forges. 

<img src="https://lium.univ-lemans.fr/wp-content/uploads/2023/10/komit-variantes-1.png" width="900">

## License
CC BY - Iza Marfisi, Bertrand Marne et Alexandra Freitas-Alves


Voici d'autres variantes.

<img src="https://lium.univ-lemans.fr/wp-content/uploads/2024/11/komit-variantes-2.png" width="900">

## License
CC BY - Iza Marfisi, Bertrand Marne et Atelier Joualle

## Contacts
2024
Contact : iza.marfisi@univ-lemans.fr
